# Changelog

## 1.9.3
### Fixes
* Texte mentions légales [#444](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/444)
* Invitation depuis compte Admin défaillant [#443](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/443)


## 1.9.0
### Features
* Voir les effectifs d'un groupe sélectionné sur mon calendrier [#419](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/419)
* Ajout détails x/y/z dans la popup activité du calendrier [#419](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/419)
### Fixes
* Adapter les stats aux personnels qui ont connus plusieurs affectations [#422](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/422)
* wording [#421](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/421)
* Une fois un participant traité ne pas le garder sélectionné [#420](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/420)
* Correction bug affichage calendrier [#419](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/419)


## 1.8.0
### Features
* Sondage des utilisateurs pour participer à des tests [#418](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/418)
### Fixes

## 1.7.0
### Features
* voir les profils utilisateur et modifié celui par default [#411](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/411)
* Partage d'activités inter-unités [#409](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/409)
### Fixes

## 1.6.0
### Features
* popup nouveautés [#404](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/404)
- Ajout du header iframe [#399](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/399)
- Ajout des fonts en local [#399](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/399)
* Annuler sa candidature et candidater à nouveau [#394](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/394)
* Modifier les paramètres de rappel d'une activité [#391](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/391)
* Quand on change de profil employeur/employé on reste sur le même mois/activité [#390](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/390)
* Distinguer les disponibles et les candidats sur les pop-in info activité [#386](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/386)
* Ajout du nom et des dates de l'activité sur la page de participation [#385](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/385)
* Ajout de la date de candidature sur la page de participation [#385](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/385)
* Amélioration du dashboard organismes et utilisateurs [#383](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/383)
### Fixes
* bug à la modification des profils [#405](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/405)
* Modifier les informations d'un pax : affichage de tous les profils du pax même ceux non modifiables [#389](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/389)
* recherche et sélection des participants [#380](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/380)

## 1.5.0
### Features
* Niveau de filtre dispo/indispo/NR dans disponibilité de mon unité [#371](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/371)
* Vue statistiques : ajout du nombre de convocations et du nombre de candidatures [#370](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/370)
* Autoriser les employeurs à modifier les noms/prénoms des pax [#368](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/368)
* L'admin brigade ou régiment peux inviter des personnes [#367](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/367)
* pages Mentions Légales [#328](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/328)
### Fixes
* Préciser incompatible IE si erreur 500 lié au navigateur [#378](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/378)
* Ordre des étiquettes de types d'activités [#372](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/372)
* Accessibilité/Contraste/Taille texte [#369](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/369)
* Invitations multiples séparées par des virgules [#365](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/365)
* fix la redirection de la page d'accueil [#364](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/364)
* Redirection vers l'URL demandée à l'origine avant d'avoir été rediriger vers la page de connexion [#363](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/363)
* Ajout d'une croix pour fermer le pop-up des dispo/indispo [#359](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/359)
* Amélioration des vues mobiles [#350](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/350)


## 1.4.2
* Ne pas afficher la popup nouveauté sur mobile

## 1.4.1
* Ne pas afficher la popup nouveauté sur mobile

## 1.4.0
### Features
* Les personnes sélectionnées restent sélectionnées tant que je suis sur le meme statut ("dispo/indispo" ou "retenue/non-retenue" ou "convoqué/non-convoqué"). [#348](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/348)
* Copier coller des adresses mail depuis excel pour les invitations [#341](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/341)
* Rendre un pax actif/inactif pour les CDU [#334](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/334)
* Ajout d'un profil super employé pour les chefs de groupe [#321](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/321)
### Fixes
* Dashboard des statistiques [#344](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/344)
* rechercher des invitations [#338](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/338)
* dates disponibilité UTC [#336](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/336)
* Suppression de l'ascenseur sur le calendrier [#326](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/326)

## 1.3.0
### Features
* photos [#320](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/320)
* Add Matomo [#312](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/312)
* Mécanisme de changement d'e-mail [#307](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/307)
* pages d'erreurs 404-403-500 [#306](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/306)
* Détail d'un pax onglet activité : ajout d'un filtre 'Activités réalisées' [#300](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/300)
* Détail d'un pax onglet activité : ajout d'un filtre sur les dates [#300](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/300)
* Détail d'un pax onglet disponibilités : ajout d'un calendrier annuel [#300](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/300)
* ajout des statistiques personnel [#299](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/299)
### Fixes
* obligation commentaire [#330](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/330)
* wording [#325](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/325)
* Affichage listes des autres activités dans la participation [#324](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/324)
* obligation commentaire [#323](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/323)
* ordre type activite [#322](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/322)
* apparition bouton dans participation [#318](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/318)
* Affichage du menu [#317](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/317)
* Rendre cliquable les initiales dans l'entête [#316](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/316)
* Suppression d'invitations [#315](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/315)
* Bouton retour en haut de la page d'aide [#314](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/314)
* filtrer la liste des pax de mon unité par groupe [#313](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/313)
* Affichage libelle trop long dans la liste des activités [#311](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/311)
* Supprimer des invitations envoyées [#310](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/310)
* Possibilité de créer plus 10 groupes [#309](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/309)
* wording [#305](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/305)
* Liste des activité : affichage des libellés long [#304](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/304)
* Dashboard utilisateur : bouton clôturer affectation en rouge [#303](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/303)
* jauge remplissage activité [#302](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/302)
* Contraste checkbox administrer activité [#301](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/301)
* Gestion des utilisateurs invités mais non inscrit [#298](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/298)
* apparition du bouton [#291](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/291)


## 1.2.0
### Features
* pop up des mises à jours [#280](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/280)
* Pax retenu sur plusieurs activités [#273](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/273)
* Ajout d'un rappel de participation [#270](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/270)
* visualiser le taux de remplissage supérieur à la demande [#269](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/269)
* ajout de bouton pour sélectionner tous les pax d'un catégorie [#268](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/268)
* popup annuler participation [#266](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/266)
* ajout du filtre groupe sur la liste des pax [#256](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/256)
* filtre des groupe sur les disponibilités [#254](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/254)
* filtre sur les disponibilité des pax  [#254](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/254)
* Ajout d'un administrateur local [#213](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/213)
### Fixes
* erreurs de cache à la connexion [#295](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/295)
* fermeture dropdown alertes [#294](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/294)
* Essai différent pour l'erreur 500/403 à la connexion [#293](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/293)
* vue annuelle : activités sous le jour + date d'activité dans le popup [#290](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/290)
* checkBox [#289](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/289)
* Création modification activité depuis le dashboard [#288](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/288)
* taux [#286](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/286)
*  pattern email [#285](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/285)
* modification du CSS [#284](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/284)
* jauge [#283](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/283)
* wording [#282](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/282)
* Fix Erreur 500 a la connexion d'adminlocal [#281](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/281)
* Supprimer les lignes vides quand on filtre "disponibles uniquement" dans les disponibilités de l'unité. [#279](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/279)
* Recharge de la liste des pax dans l'écran des disponibilités de mon unité en mode admin ou admin local quand on change d'unité. [#279](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/279)
* Garder le mois et l'année retour sur le calendrier depuis une activité [#278](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/278)
* wording [#277](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/277)
* Fermeture de la liste des alertes au click sur une activité [#276](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/276)
* Daschboard création/modification organisme : ajouter le régiment dans la liste des unités [#275](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/275)
* Vérification du format des emails pour les invitations et la réinitialisation du mot de passe [#274](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/274)
* commentaire candidater Facultatif [#272](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/272)
* visualisation du commentaire saisie [#271](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/271)
* Manque l'organisme pour l'invitation en mode employeur [#267](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/267)
* Disclaimer à l'inscription (ne pas saisir de grade) [#265](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/265)
* CSS calendrier [#264](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/264)
* Ajout d'un pattern de saisie des mails [#263](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/263)
* "Disponible" vers "Disponibles uniquement" [#262](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/262)
* Pouvoir désactivé les filtre OFF, SO, MDR sur détails activité [#261](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/261)
* saisie description [#260](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/260)
* Wording [#258](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/258)
* Calendrier en mode français [#257](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/257)
* Pouvoir modifier un poste RO en champ vide [#255](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/255)
* Enlever le "autre" dans la liste des postes RO [#255](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/255)
* Dans modifier les information d'un PAX afficher le libelle à la place du code pour le poste RO [#255](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/255)
* Le message d'erreur "token désactivé" persiste même une fois connecté. [#253](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/253)
* Amélioration de la gestion des emails incorrectes et des rejets. [#252](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/252)

## 1.1.0
### Features
* Amélioration IHM commentaire disponibilité [#248](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/248)
* amélioration de la page des contact [#244](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/244)
* Le bouton retour activité renvoie vers le bon calendrier. [#242](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/242)
* Disponibilité sur le calendrier annuelle [#241](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/241)
* Amélioration de la navigations dans l'écran des groupes [#240](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/240)
* Statistiques dernières connexions [#238](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/238)
* Information supplémentaire sur la pop-up activité du calendrier. [#237](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/237)
* Calendrier annuel [#234](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/234)
* Ajout de statistiques [#233](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/233)
* Améliorations des alertes [#232](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/232)
* le bouton retour sur l'écran activité, reviens sur le calendrier au mois du debut de l'activité. [#231](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/231)
* Amélioration de l'écran des duplication d'activités [#228](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/228)
* Page d'aide [#227](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/227)
* Mise en place de l'écran des nouveautés [#226](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/226)
* Possibilité d'envoyer automatiquement un rappel quelque jour avant le début d'une activité [#225](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/225)
* Possibilité d'envoyer un mail à toute l'unité lors de la création d'une activité. [#225](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/225)
* possibilité de voir les commentaire des disponibilités et indisponibilités [#220](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/220)
* pourvoir transformer 'non retenus' , 'non convoqués' , 'non réalisé' -> 'retenu' , 'convoqué' , 'realisé' [#215](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/215)
* Participer à plusieurs activités [#215](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/215)
* Impossibilité d'etre retenu sur plusieurs activité [#215](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/215)
* Candidater [#199](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/199)
* Se desinscrire [#199](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/199)
* Remise en place de la gestion des groupes [#197](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/197)
### Fixes
* No limites for Organisme [#249](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/249)
* Correction bug commentaire page d'activité [#248](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/248)
* corrections bugs commentaire [#247](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/247)
* commentaire disponibilité sur le calendrier [#243](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/243)
* Corrections de bugs (bouton & variables plus utilisé) [#241](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/241)
* Corrections orthographiques [#240](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/240)
* Correction IHM de l'écran des groupes [#240](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/240)
* Corrections wording aide [#240](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/240)
* Mes activités est à nouveau accessible. [#239](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/239)
* 404 favicon [#235](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/235)
* lireAlerte plus utilisé [#235](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/235)
* props style reservé [#235](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/235)
* Probleme disponible > non retenu [#215](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/215)
* impossibilité de saisir une date de fin supérieur à la date de début pour une activité [#214](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/214)
* email pour changer de mot de passe  [#207](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/207)
* erreur si email inconnue [#207](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/207)
## 1.0.3
### Features
* Fermeture du menu au clic [#223](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/223)
* Export csv des participants à une activité [#219](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/219)
* Renvoyer des mails d'inscription sans attendre l'expiration [#212](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/212)
* Amélioration des bouton de type d'activité sur le calendrier (Survol) [#203](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/203)
* Détails utilisateur dans l'écran des statistique organismes / utilisateurs [#201](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/201)
* Les dates 'suivent' d'un écran à l'autre (organismes -> utilisateurs, utilisateurs -> organismes) [#201](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/201)
* Décompté du nombre d'utilisateurs affiché. [#201](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/201)
* clôturer utilisateur [#200](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/200)
* Info bulles front (prêt mais à ne merge que apres v1) [#175](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/175)
### Fixes
* Correction de l'affichage des event du calendier suite à la maj version [#222](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/222)
* wording [#218](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/218)
* Ajout de la date de création des messages [#217](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/217)
* date du calendrier sur l'écran Informations personnelles d'un pax [#216](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/216)
* Dates éditables manuellement [#210](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/210)
* Passer à 300 la limite du nombres de caractères dans les commentaires [#208](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/208)
* Modifier mes infos persos : renouvellement du token pour éviter une erreur en cas de changement de username. [#206](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/206)
* filtre sur activé/désactivé utilisateur [#200](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/200)
* wording [#200](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/200)
## 1.0.2
### Features
* Lors de la fermeture de la pop-up de création d'activité (création / duplication) via la croix : renvoie vers la page de visualisation. [#198](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/198)
### Fixes
* Typo `*` dans dupliquer.vue [#202](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/202)
* L'utilisateur doit avoir au moins un profil [#196](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/196)
* Curseur 'pointeur' pour le composant mt-utilisateur [#193](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/193)
## 1.0.1
### Features
* Icone pour mobile [#192](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/192)
* Augmentation taille messages alerte (pour que les message de suppression & de modification soit lisible) [#191](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/191)
* Amelioration de l'écrans des statistique Organismes [#188](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/188)
* Création d'un écran de statistiques utilisateurs [#188](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/188)
### Fixes
* les liens utilisant le mt-utilisateur.vue [#191](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/191)
* Erreur 404 sur les filtreWithGroupe qui n'aurait pas du être appelé en mode employé [#191](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/191)
* Alertes : suppression des liens vers les alertes supprimé. [#191](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/191)
* Bug Menu super employeur / employeur [#189](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/189)
## 1.0.0
### Features
* Pop up info-user dans l'écran des activités (participants) [#185](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/185)
* Optimisation de l'écran des participations pour mobile [#184](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/184)
* Ajout A Propos redirection vers https://beta.gouv.fr/startups/metiis.html [#182](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/182)
* Nous évaluer : Note sur 10 [#181](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/181)
* Nous évaluer : Date de création de l'évaluation [#181](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/181)
* Bouton 'Annuler ma participation' [#180](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/180)
* Redirige vers le bon profil, lorsqu'un compte connecte essaye d'acceder a une URL d'un profil dont il n'a pas les droits [#179](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/179)
* Information sur le champs descriptions de la création d'activité [#177](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/177)
* Maj écran statistiques activités : ajout du nombre d'activités crée pendant la période [#176](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/176)
* Persiste le mois selectionne grace a l'URL entre la vue calendrier et la vue liste [#170](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/170)
* Mise en place profil Super Employeur [#168](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/168)
* Optimisation du site pour mobile [#164](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/164)
* Modification de mes infos persos [#154](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/154)
* commentaire sur la suppression [#153](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/153)
* ajout bouton dupliquer sur l’édition d’activité [#153](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/153)
* champ date non modifiable dans l'édition  [#153](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/153)
* Gestion des groupes personnalisé [#152](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/152)
* Gestion des groupes dans les activités [#152](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/152)
* Ordre dans les poste au RO [#150](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/150)
* Affichage du poste au RO à la place du grade [#150](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/150)
* Details utilisateurs dans l'écran des pax (info perso / activités / dispo) [#150](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/150)
* Modifier informations utilisateurs en tant qu'employeur [#150](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/150)
* Mixin pour les jours / mois fr (+ refacto) [#150](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/150)
* Composant pour affiché un utilisateur (+ refacto) [#150](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/150)
* Composant puce dispo (+ refacto) [#150](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/150)
* Ecran invitation [#147](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/147)
* Prise en compte de droit creation profil. [#147](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/147)
* Ajout du tri par colonne et filtre sur la liste des activités [#146](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/146)
* Ajout des colonnes statut et disponibilité sur la liste des activités employé [#146](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/146)
* Ajout du filtre "Mes activités" sur la liste des activités employé et sur le calendrier employé [#146](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/146)
* Différentiation de mes activités sur le calendrier employé [#146](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/146)
* Inscription avec Token [#145](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/145)
* Mot de passe oublié [#145](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/145)
* Nouveau logo [#144](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/144)
* retrait de grade  [#143](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/143)
* Ajout tri et filtre sur le dashboard utilisateurs [#140](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/140)
* DashBoard Etape [#139](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/139)
* Formulaire de contact [#138](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/138)
* DashBorad message des contact [#138](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/138)
* Gestion des participants [#135](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/135)
* Filtre calendrier [#135](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/135)
* Formulaire d'évaluation [#122](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/122)
* DashBorad des messages des évaluations [#122](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/122)
* Ecran d'édition des droits de création d'un profil [#66](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/66)
* Alertes activités [#65](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/65)
* Lien vers les disponibilites de l'unite depuis le calendrier
* Composant générique modal
* Composant modal 'ok'
* Composant modal 'action' (avec type-delete)
* Composant modal 'triple actions'
* Composant modale 'Poursuivre / Quitter'
* Mise à jour de la page de création des activités avec les modals
* Mise à jour composant Page title : ajout validation / style / padding pré calibré
* Mise à jour du header
* Mise à jour du Footer
* Mise à jour du fond (composant Container)
* Dashboard profils
* Dashboard utilisateurs
* Mise à jour page d'inscription / connection
* Ajout des effets sur les boutons (survole, etc....)
* Ecran de disponibilite utilisateurs
* Creation composant message info
* Ajout du popup "Nous contacter" (sur crise, enlevé sur develop)
* Contexte employe
* Déclaration disponibilites pour le statut employe
* Ajout d'un disclaimer sur la page de login (sur crise, enlevé sur develop)
* Ajout d'un popup sur le lien "mot de passe oublié" (sur crise, enlevé sur develop)
* Ecran d'affichage des informations personnelles de l'utilisateur connecté
* Cacher les liens en fonction du profil
* Ecran de changement du mot de passe de l'utilisateur
* Modification du mot de passe depuis le dashboard/modification utilisateur
* Commentaire enlevé sur écran de création des disponibilités
* Ajout d'une confirmation de suppression pour les objets du dashboard
* Ecran de changement du mot de passe de l'utilisateur
* Ajout d'un message d'erreur lorsque les dates du datepicker sont inversees
* Gestion des exceptions pour récupérer message du back
* Modification du texte du poup de confirmation d'inscription
* Ajout des flèches de changement de mois sur le calendrier (+ gestion de la vue mobile)
* Ajout d'une interface admin pour envoyer un mail aux utilisateurs dont le compte a été activé/débloqué
* Ajout d'un voile bleuté derrière les popups
* Ajout grade dans liste des pax
* Dashboard - tri des utilisateurs par nom - 20 utilisateurs par page
* Ajout de la cloche des alertes, d'alertes à la création/modif/suppr d'une activité, d'une page 'Mes alertes'
### Fix
* Correction du bug css des boutons "chevrons" sous chrome. [#184](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/184)
* Fix de la taille du texte d'un evenement lors du dernier jour qui est en debut de semaine (la progression disparaissait si le titre etait trop long) [#183](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/183)
* Nous évaluer : Correction ihm [#181](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/181)
* Pax unité (employeur) [#181](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/181)
* Fix de l'experience utilisateurs pour les invitations [#178](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/178)
* Le menu ‘nous contacter’ ne fonctionne pas quand on est connecté. [#174](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/174)
* Fix de la position des evenements lors du passage a une nouvelle semaine [#173](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/173)
* Fix du profil selectionne a la connexion [#172](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/172)
* Correction de bug graphique sur mobile [#171](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/171)
* Correction de bug de champs date sur mobile [#171](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/171)
* Fix du profil selectionne en fonction de l'URL [#169](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/169)
* Fix du warn dans la vue detail d'une activite (array au lieu d'Object) [#167](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/167)
* Changement du message d'erreur de connexion sur le mot de passe ou l'identifiant  [#166](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/166)
* Fix de la position des activites en fin de mois [#165](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/165)
* Un employé ne doit pas pouvoir se mettre indisponible sur une date qui tombe pendant une activité sur laquelle il est déjà retenu ou convoqué [#163](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/163)
* description non obligatoire [#162](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/162)
* "Profil" devient "Mon compte" [#161](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/161)
* "Modifier l'activité" devient "Administrer l'activité" [#161](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/161)
* Divers wording [#161](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/161)
* Fix invitation non operationnel pour le compte employé [#160](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/160)
* Fix css  [#160](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/160)
* Fix nombre d'alerte non affiché [#160](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/160)
* Menu qui disparait [#159](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/159)
* Retour à l'accueil renvoit sur le calendrier dans l'écran détail d'une activité [#158](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/158)
* Ajout d'un chevron sur la colonne triée dans la liste des activités [#157](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/157)
* CSS titre des colonnes dans la liste des activités [#157](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/157)
* Date en français dans l'écran de création d'activité [#156](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/156)
* Message d'erreur quand la date de fin est antérieure à la date de début dans l'écran de création d'activité [#156](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/156)
* Remplacement de "Effectif prévisionnel" par "Effectif potentiel" dans l'écran de création d'activité [#156](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/156)
* Correction de l'écran de changement de mot de passe du profil [#155](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/155)
* Fix gitlab-ci [#149](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/149)
* Fixe activités sur le calendrier [#135](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/135)
* Flèches du calendrier (mois suivant/précédent)  mal placé en mode liste activité. [#135](https://gitlab.com/fabnum-minarm/metiis/frontend/-/merge_requests/135)
* Profil par defaut* Header quand deconnecte
* Mois en français dans datePicker
* Popup dans le calendar
* Wording
* Suppression d'un retour à ligne inélégant dans le fil de fer (lien "retour...") sur téléphone
* mise en place de la couleur "non renseigné" sur le calendrier employeur
* mise en responsive du disclaimer
* DatePicker en français
* Affichage mobile liste des pax
* Uniquement les pax activés dans la liste des pax
## 0.2.0
### Features
* Ajout de la page d'inscription
* Mise à jour de la page de création des activités
* Mise à jour de la page de modification des activités
* Mise à jour composant Page title
* Dashboard types d'activités
* Dashboard type disponilité
* Page Accessibilite
### Fix
* Dates consistente que l'on vienne du calendrier ou du picker de date
* Fix du bug sur le calendrier lors du click sur une activité apres avoir sélectionné un jour de début
## 0.1.0
### Features
* Ajout du changelog
* Dashboard organismes
* Dashboard types d'organismes
* Dashboard grade
* Dashboard corps
* Nouveau design calendrier
* Nouveau design liste activites (table)
### Fix
