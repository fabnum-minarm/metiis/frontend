import {
  convertirActiviteDateObjectToString,
  convertirActiviteDateStringToObject,
} from '~/components/mixins/activityMixin';

const entite = '/v1/activite';

export default (axios) => ({
  async get(id) {
    const activite = await axios.$get(`${entite}/${id}/`);
    return new Promise((resolve) => resolve(convertirActiviteDateStringToObject(activite)));
  },
  async getFromParent(token) {
    const activite = await axios.$get(`${entite}/${token}/from-parent`);
    return new Promise((resolve) => resolve(convertirActiviteDateStringToObject(activite)));
  },
  async getParent(id) {
    const activite = await axios.$get(`${entite}/${id}/parent`);
    return new Promise((resolve) => resolve(convertirActiviteDateStringToObject(activite)));
  },
  async getEnfants(id) {
    let activites = await axios.$get(`${entite}/${id}/enfants`);
    activites = activites.map((activite) => convertirActiviteDateStringToObject(activite));
    return new Promise((resolve) => resolve(activites));
  },
  detailsPopUp(id) {
    return axios.$get(`${entite}/${id}/details-participation-pop-up`);
  },
  async post(data) {
    const activite = await axios.$post(`${entite}`, convertirActiviteDateObjectToString(data));
    return new Promise((resolve) => resolve(convertirActiviteDateStringToObject(activite)));
  },
  async put(id, data) {
    const activite = await axios.$put(`${entite}/${id}/full`, convertirActiviteDateObjectToString(data));
    return new Promise((resolve) => resolve(convertirActiviteDateStringToObject(activite)));
  },
  async modifierPartiellement(id, data) {
    const activite = await axios.$patch(`${entite}/${id}/`, convertirActiviteDateObjectToString(data));
    return new Promise((resolve) => resolve(convertirActiviteDateStringToObject(activite)));
  },
  etapesStatuts(id, idEtape, idGroupe) {
    return axios.$get(`${entite}/${id}/etapes/${idEtape}/etape-statuts`, {
      params: {
        idGroupe,
      },
    });
  },
  exportParticipants(id, idEtape, idGroupe, idEtapeStatut) {
    return axios({
      url: `${entite}/${id}/etapes/${idEtape}/${idEtapeStatut}/export-participants/`,
      params: {
        idGroupe,
      },
      method: 'GET',
      responseType: 'blob', // important
    });
  },
  commentaireEmployeur(id) {
    return axios.$get(`${entite}/${id}/commentaire/employeur`);
  },
  mesCommentairesEmploye(id) {
    return axios.$get(`${entite}/${id}/commentaire/employe`);
  },
  commentaireEmploye(id, idUtilisateur) {
    return axios.$get(`${entite}/${id}/commentaire/employe/${idUtilisateur}`);
  },
  commentairesDeLemploye(id, idUtilisateur, idEtape) {
    return axios.$get(`${entite}/${id}/commentaire/employe-only/${idUtilisateur}/etape/${idEtape}`);
  },
  rechercheParticipants(id, search) {
    return axios.$get(`${entite}/${id}/recherche-participant`, {
      params: {
        search,
      },
    });
  },
  statut(id, idStatut, idCorps, idGroupe) {
    return axios.$get(`${entite}/${id}/statut/${idStatut}`, {
      params: {
        idCorps,
        idGroupe,
      },
    });
  },
  delete(id, commentaire) {
    return axios.$delete(`${entite}/${id}/`, {
      params: {
        commentaire,
      },
    });
  },
  async all(page = 0, size = 30) {
    const res = await axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
    res.content = res.content.map((activite) => convertirActiviteDateStringToObject(activite));
    return new Promise((resolve) => resolve(res));
  },
  nombreDePax(idOrganisme) {
    return axios.$get(`${entite}/organisme/${idOrganisme}/`);
  },
  async partage(id, organismes) {
    const activite = await axios.$post(`${entite}/${id}/partager`, organismes);
    return new Promise((resolve) => resolve(convertirActiviteDateStringToObject(activite)));
  },
});
