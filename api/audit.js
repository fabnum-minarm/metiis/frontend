const entite = '/v1/audit';

export default (axios) => ({
  utilisateur(id) {
    return axios.$get(`${entite}/utilisateur/${id}`);
  },
});
