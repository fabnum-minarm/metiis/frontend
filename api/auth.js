export default (axios) => ({
  changePassword(data) {
    return axios.post('/v1/auth/changePassword', data);
  },
  me() {
    return axios.get('/v1/auth/me');
  },
  renew() {
    return axios.get('/v1/auth/renew');
  },
});
