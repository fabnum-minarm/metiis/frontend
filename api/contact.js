const entite = '/v1/contacts';

export default (axios) => ({
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  lire(id) {
    return axios.$patch(`${entite}/${id}/lire`);
  },
  traiter(id) {
    return axios.$patch(`${entite}/${id}/traite`);
  },
  old(page = 0, size = 30) {
    return axios.$get(`${entite}/old`, {
      params: {
        page,
        size,
      },
    });
  },
  nonLus() {
    return axios.$get(`${entite}/non-lus`);
  },
  nonTraites() {
    return axios.$get(`${entite}/non-traites`);
  },
});
