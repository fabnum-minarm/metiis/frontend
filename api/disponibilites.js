import { convertirDateDisponibiliteStringToObject } from '~/components/mixins/disponibiliteMixin';

const entite = '/v1/disponibilite';

export default (axios) => ({
  async disponibilitesCommentaireEntre(idUtilisateur, dateDebut, dateFin) {
    const disponibilites = await axios.$get(
      `${entite}/${idUtilisateur}/liste-commentaire-disponibilite/entre/${dateDebut}/${dateFin}`,
    );
    return new Promise((resolve) =>
      resolve(disponibilites.map((dispo) => convertirDateDisponibiliteStringToObject(dispo))),
    );
  },

  disponibiliteEntre(idUtilisateur, dateDebut, dateFin) {
    return axios.$get(`${entite}/${idUtilisateur}/disponibilite/entre/${dateDebut}/${dateFin}`);
  },

  delete(id, dateDebut, dateFin) {
    return axios.$delete(`${entite}/${id}/suprimer-disponiblilite/entre/${dateDebut}/${dateFin}`);
  },
});
