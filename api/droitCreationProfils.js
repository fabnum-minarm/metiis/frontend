const entite = '/v1/droit-creation-profil';

export default (axios) => ({
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  delete(idCreateur, idCree) {
    return axios.$delete(`${entite}/${idCreateur}/${idCree}`);
  },
  allProfilWithDroit() {
    return axios.$get(`${entite}/all-profils-with-droit`);
  },
  allCreateur(idCreateur) {
    return axios.$get(`${entite}/all-createur/${idCreateur}/`);
  },
});
