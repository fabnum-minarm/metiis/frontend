const entite = '/v1/etape';

export default (axios) => ({
  employeur() {
    return axios.$get(`${entite}/employeur`);
  },
  employe() {
    return axios.$get(`${entite}/employe`);
  },
  all() {
    return axios.$get(`${entite}`);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}/`, data);
  },
  get(id) {
    return axios.$get(`${entite}/${id}/`);
  },
});
