const entite = '/v1/evaluation';

export default (axios) => ({
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  async allPage(page = 0, size = 30) {
    const res = await axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
    return res;
  },
  all() {
    return axios.$get(`${entite}/all`);
  },
  moyenneUtilisation() {
    return axios.$get(`${entite}/moyenne-utilisation`);
  },
  moyenneBesoin() {
    return axios.$get(`${entite}/moyenne-besoin`);
  },
});
