const entite = '/v1/grade';

export default (axios) => ({
  all(page = 0, size = 30) {
    return axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
  },
  get(id) {
    return axios.$get(`${entite}/${id}/`);
  },
  byCorps(id) {
    return axios.$get(`${entite}/corps/${id}/`);
  },
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}/`, data);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}/`);
  },
});
