const entite = '/v1/groupe-personnaliser';

export default (axios) => ({
  get(id) {
    return axios.$get(`${entite}/${id}`);
  },
  utilisateurs(id, idCorps = null, search = null) {
    return axios.$get(`${entite}/${id}/utilisateur-filter`, {
      params: {
        idCorps,
        search,
      },
    });
  },
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}`, data);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}/`);
  },
});
