import qs from 'qs';
import auth from './auth';
import organismes, { organisme } from '~/api/organismes';
import typesOrganismes from '~/api/typesOrganismes';
import utilisateurs, { utilisateur } from '~/api/utilisateurs';
import grades from '~/api/grades';
import corps from '~/api/corps';
import typesActivites from '~/api/typesActivites';
import activites from '~/api/activites';
import postesRo from '~/api/postesRo';
import profils from '~/api/profils';
import users from '~/api/users';
import typesDisponibilites from '~/api/typesDisponibilites';
import evaluations from '~/api/evaluations';
import contact from '~/api/contact';
import etapes from '~/api/etapes';
import participer from '~/api/participer';
import tokenUtilisateur from '~/api/tokenUtilisateur';
import droitCreationProfils from '~/api/droitCreationProfils';
import audit from '~/api/audit';
import groupePersonnaliser from '~/api/groupePersonnaliser';
import statistiques from '~/api/statistique';
import nouveautes from '~/api/nouveautes';
import disponibilites from '~/api/disponibilites';
import songage from '~/api/sondage';
import notification from '~/api/notification';

export default function(ctx, inject) {
  ctx.$axios.defaults.paramsSerializer = (params) => {
    return qs.stringify(params, { arrayFormat: 'repeat' });
  };
  const api = {
    auth: auth(ctx.$axios),
    organismes: organismes(ctx.$axios),
    typesOrganismes: typesOrganismes(ctx.$axios),
    organisme: organisme(ctx.$axios, ctx.store),
    utilisateurs: utilisateurs(ctx.$axios),
    utilisateur: utilisateur(ctx.$axios, ctx.store),
    corps: corps(ctx.$axios),
    grades: grades(ctx.$axios),
    typesActivites: typesActivites(ctx.$axios),
    activites: activites(ctx.$axios),
    postesRo: postesRo(ctx.$axios),
    profils: profils(ctx.$axios),
    users: users(ctx.$axios),
    typesDisponibilites: typesDisponibilites(ctx.$axios),
    contact: contact(ctx.$axios),
    etapes: etapes(ctx.$axios),
    participer: participer(ctx.$axios),
    droitCreationProfils: droitCreationProfils(ctx.$axios),
    tokenUtilisateur: tokenUtilisateur(ctx.$axios),
    audit: audit(ctx.$axios),
    groupePersonnaliser: groupePersonnaliser(ctx.$axios),
    evaluations: evaluations(ctx.$axios),
    statistiques: statistiques(ctx.$axios),
    disponibilites: disponibilites(ctx.$axios),
    nouveautes: nouveautes(ctx.$axios),
    sondage: songage(ctx.$axios),
    notification: notification(ctx.$axios),
  };
  ctx.$api = api;
  inject('api', api);
}
