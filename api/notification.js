const entite = '/v1/notification';

export default (axios) => ({
  get(id) {
    return axios.$get(`${entite}/utilisateur/${id}`);
  },
  modifierNotification(id, data) {
    return axios.$put(`${entite}/modifier-notification/${id}`, data);
  },
});
