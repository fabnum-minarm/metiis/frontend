const entite = '/v1/nouveautes';

export default (axios) => ({
  me() {
    return axios.$get(`${entite}/me`);
  },
  readAll() {
    return axios.$get(`${entite}/read-all`);
  },
});
