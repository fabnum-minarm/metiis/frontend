import { convertirActiviteDateStringToObject } from '~/components/mixins/activityMixin';
import { convertirDateAffectationStringToObject } from '~/components/mixins/utilisateurMixin';

const entite = '/v1/organisme';

export default (axios) => ({
  get(id) {
    return axios.$get(`${entite}/${id}/`);
  },
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}/`, data);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}/`);
  },
  allPageable(page = 0, size = 30) {
    return axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
  },
  all() {
    return axios.$get(`${entite}/all`);
  },
  allByTypeOrganisme(idTypeOrganisme) {
    return axios.$get(`${entite}/type-organisme/${idTypeOrganisme}/`);
  },
  organismesByParent(idOrganismeParent) {
    return axios.$get(`${entite}/${idOrganismeParent}/enfants/`);
  },
  affectableAllowed() {
    return axios.$get(`${entite}/organismes-affectable-allowed`);
  },
  organismesByParentAndTypeOrganisme(idTypeOrganisme) {
    return axios.$get(`${entite}/enfants-by-type-organisme/${idTypeOrganisme}`);
  },
  allAllowed() {
    return axios.$get(`${entite}/all-organismes-allowed/`);
  },
  utilisateurs(id) {
    return axios.$get(`${entite}/${id}/utilisateurs/`);
  },
  organismesAllowedForPartageActivite(idActivite) {
    return axios.$get(`${entite}/organismes-allowed-partage-activite/${idActivite}`);
  },
  allHiearchie() {
    return axios.$get(`${entite}/hiearchie/`);
  },
  async allUtilisateursSort(id, page = 0, size = 30, sortBy = 'nom', order = 'ASC', filtre = '', enabled) {
    const res = await axios.$get(`${entite}/${id}/utilisateurs-sort`, {
      params: {
        page,
        size,
        sortBy,
        order,
        filtre,
        enabled,
      },
    });
    res.content = res.content.map((util) => convertirDateAffectationStringToObject(util));
    return new Promise((resolve) => resolve(res));
  },
});

export function organisme(axios, store) {
  const defaultId = () => {
    return store.getters['context/organisme'] && store.getters['context/organisme'].id
      ? store.getters['context/organisme'].id
      : null;
  };

  return (id = defaultId()) => ({
    async activitesEntre(dateDebut, dateFin, idTypeActivite, idGroupe, filtre) {
      if (id === null) {
        return new Promise((resolve) => resolve([]));
      }
      const activites = await axios.$get(`${entite}/${id}/activite/entre/${dateDebut}/${dateFin}`, {
        params: {
          idTypeActivite,
          idGroupe,
          filtre,
        },
      });
      return new Promise((resolve) =>
        resolve(activites.map((activite) => convertirActiviteDateStringToObject(activite))),
      );
    },
    activiteAnnuel(dateDebut, idTypeActivite) {
      return axios.$get(`${entite}/${id}/activite-annuel/${dateDebut}`, {
        params: {
          idTypeActivite,
        },
      });
    },
    id() {
      return defaultId();
    },
    disponibilitesComplete(dateDebut, dateFin, idCorps) {
      return axios.$get(`${entite}/${id}/disponibilite-complete/entre/${dateDebut}/${dateFin}`, {
        params: {
          idCorps,
        },
      });
    },
    organismesByEnfantOrderByHierarchie() {
      return axios.$get(`${entite}/${id}/parents-by-hierarchie/`);
    },
    disponibilitesEntre(dateDebut, dateFin) {
      if (id === null) {
        return new Promise((resolve) => resolve([]));
      }
      return axios.$get(`${entite}/${id}/disponibilite/entre/${dateDebut}/${dateFin}`);
    },
    async allSort(page = 0, size = 30, sortBy = 'nom', order = 'ASC', filtre = '', enabled) {
      const res = await axios.$get(`${entite}/${id}/utilisateurs-sort`, {
        params: {
          page,
          size,
          sortBy,
          order,
          filtre,
          enabled,
        },
      });
      res.content = res.content.map((util) => convertirDateAffectationStringToObject(util));
      return new Promise((resolve) => resolve(res));
    },
    utilisateursBloques(idCorps) {
      return axios.$get(`${entite}/${id}/utilisateurs-bloques`, {
        params: {
          idCorps,
        },
      });
    },
    utilisateursAValider(idCorps) {
      return axios.$get(`${entite}/${id}/utilisateurs-a-valider`, {
        params: {
          idCorps,
        },
      });
    },
    exportDisponibilitesComplete(dateDebut, dateFin) {
      return axios({
        url: `${entite}/${id}/export-disponibilite-complete/entre/${dateDebut}/${dateFin}`,
        method: 'GET',
        responseType: 'blob', // important
      });
    },

    exportDisponibilites(dateDebut, dateFin) {
      return axios({
        url: `${entite}/${id}/export-disponibilite/entre/${dateDebut}/${dateFin}`,
        method: 'GET',
        responseType: 'blob', // important
      });
    },
    utilisateursFilter(idCorps, idGroupe) {
      return axios.$get(`${entite}/${id}/utilisateurs-filter/`, {
        params: {
          idCorps,
          idGroupe,
        },
      });
    },
    utilisateursFilterWithNoEnabled(idCorps, idGroupe) {
      return axios.$get(`${entite}/${id}/utilisateurs-filter/with-no-enabled`, {
        params: {
          idCorps,
          idGroupe,
        },
      });
    },
    utilisateursFilterWithMyGroupes(idCorps, idGroupe) {
      return axios.$get(`${entite}/${id}/utilisateurs-filter/with-my-groupe`, {
        params: {
          idCorps,
          idGroupe,
        },
      });
    },
    utilisateursFilterWithMyGroupesAndNoEnabled(idCorps, idGroupe) {
      return axios.$get(`${entite}/${id}/utilisateurs-filter/with-my-groupe/with-no-enabled`, {
        params: {
          idCorps,
          idGroupe,
        },
      });
    },
    rechercheUtilisateurs(search) {
      return axios.$get(`${entite}/${id}/recherche-utilisateurs/`, {
        params: {
          search,
        },
      });
    },
    async activites(page = 0, size = 10) {
      const res = await axios.$get(`${entite}/${id}/activite`, {
        params: {
          page,
          size,
        },
      });
      res.content = res.content.map((activite) => convertirActiviteDateStringToObject(activite));
      return new Promise((resolve) => resolve(res));
    },
    invitations() {
      return axios.$get(`${entite}/${id}/invitations`);
    },
    rechercheInvitations(search) {
      return axios.$get(`${entite}/${id}/recherche-invitations`, {
        params: {
          search,
        },
      });
    },
  });
}
