const entite = '/v1/participer';

export default (axios) => ({
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  monChoix(data) {
    return axios.$post(`${entite}/mon-choix`, data);
  },
  autresActivites(idActivite, idUtilisateur) {
    return axios.$get(`${entite}/activite/${idActivite}/autres-activites-utilisateur/${idUtilisateur}`);
  },

  participationUtilisateur(idActivite, idUtilisateur) {
    return axios.$get(`${entite}/activite/${idActivite}/utilisateur/${idUtilisateur}`);
  },
});
