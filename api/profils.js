const entite = '/v1/profil';

export default (axios) => ({
  all(page = 0, size = 30) {
    return axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
  },
  full() {
    return axios.$get(`${entite}/full`);
  },
  get(id) {
    return axios.$get(`${entite}/${id}`);
  },
  getDefault() {
    return axios.$get(`${entite}/default`);
  },
  getDefaultSimple() {
    return axios.$get(`${entite}/default-simple`);
  },
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}`, data);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}`);
  },
});
