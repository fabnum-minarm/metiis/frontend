const entite = '/v1/sondage';

export default (axios) => ({
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  dernierSondage() {
    return axios.$get(`${entite}`);
  },
  reponsesPossibles() {
    return axios.$get(`${entite}/reponses-possibles`);
  },
  allReponsesSort(idOrganisme = null, page = 0, size = 30, sortBy = 'nom', order = 'ASC', filtre = '', reponse) {
    return axios.$get(`${entite}/reponse-sondage`, {
      params: {
        page,
        size,
        sortBy,
        order,
        filtre,
        idOrganisme,
        reponse,
      },
    });
  },
});
