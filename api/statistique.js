const entite = '/v1/statistiques';

export default (axios) => ({
  utilisateurParOrganisme(idOrganisme, dateDebut, dateFin) {
    return axios.$get(`${entite}/organisme/${idOrganisme}/entre/${dateDebut}/${dateFin}`);
  },
  organisme(dateDebut, dateFin) {
    return axios.$get(`${entite}/organisme/entre/${dateDebut}/${dateFin}`);
  },
  generales() {
    return axios.$get(`${entite}/generales`);
  },
  dernieresConnexion() {
    return axios.$get(`${entite}/dernieres-connexions`);
  },
  statistiqueUtilisateur(dateDebut, dateFin, idUtilisateur) {
    return axios.$get(`${entite}/utilisateur/${idUtilisateur}/entre/${dateDebut}/${dateFin}`);
  },
});
