const entite = '/v1/token-utilisateur';

export default (axios) => ({
  all() {
    return axios.$get(`${entite}/all`);
  },
  rechercheInvitations(search) {
    return axios.$get(`${entite}/recherche-invitations`, {
      params: {
        search,
      },
    });
  },
});
