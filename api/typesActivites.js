const entite = '/v1/type-activite';

export default (axios) => ({
  allPage(page = 0, size = 30) {
    return axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
  },
  all() {
    return axios.$get(`${entite}/all`);
  },
  allOrder() {
    return axios.$get(`${entite}/all-order`);
  },
  get(id) {
    return axios.$get(`${entite}/${id}`);
  },
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}`, data);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}`);
  },
});
