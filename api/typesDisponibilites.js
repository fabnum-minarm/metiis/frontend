const entite = '/v1/type-disponibilite';

export default (axios) => ({
  get(id) {
    return axios.$get(`${entite}/${id}/`);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}/`, data);
  },
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}/`);
  },
  async allPage(page = 0, size = 30) {
    const res = await axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
    return res;
  },
  all() {
    return axios.$get(`${entite}/all`);
  },
});
