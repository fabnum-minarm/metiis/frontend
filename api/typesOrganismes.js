const entite = '/v1/type-organisme';

export default (axios) => ({
  all() {
    return axios.$get(`${entite}`);
  },
  get(id) {
    return axios.$get(`${entite}/${id}/`);
  },
  post(data) {
    return axios.$post(`${entite}`, data);
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}/`, data);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}/`);
  },
  allWithFirstChildOrganisme() {
    return axios.$get(`${entite}/with-first-child`);
  },
  childByOrganisme() {
    return axios.$get(`${entite}/child-by-organisme/`);
  },
  typeOrganismesAllowed(codeProfil) {
    return axios.$get(`${entite}/type-organismes-allowed-by-profil/${codeProfil}`);
  },
});
