import { convertirDateActivationStringToObject } from '~/components/mixins/utilisateurMixin';

const entite = '/v1/utilisateurs';

export default (axios) => ({
  roles() {
    return axios.$get(`${entite}/roles`);
  },
  async comptesActifs(page = 0, size = 30) {
    const res = await axios.$get(`${entite}/comptes-actifs`, {
      params: {
        page,
        size,
      },
    });
    res.content = res.content.map((util) => convertirDateActivationStringToObject(util));
    return new Promise((resolve) => resolve(res));
  },
});
