import {
  convertirDateAffectationStringToObject,
  convertirDateAffectationObjectToString,
  convertirDateFinAffectationObjectToString,
} from '~/components/mixins/utilisateurMixin';
import { convertirActiviteDateStringToObject, addMonStatut } from '~/components/mixins/activityMixin';

const entite = '/v1/utilisateur';

export default (axios) => ({
  async get(id) {
    const util = await axios.$get(`${entite}/${id}`);
    return new Promise((resolve) => resolve(convertirDateAffectationStringToObject(util)));
  },
  async getWithMyGroupe(id) {
    const util = await axios.$get(`${entite}/${id}/with-my-groupe`);
    return new Promise((resolve) => resolve(convertirDateAffectationStringToObject(util)));
  },
  async all(page = 0, size = 30) {
    const res = await axios.$get(`${entite}`, {
      params: {
        page,
        size,
      },
    });
    res.content = res.content.map((util) => convertirDateAffectationStringToObject(util));
    return new Promise((resolve) => resolve(res));
  },
  async allDashboardSort(page = 0, size = 30, sortBy = 'nom', order = 'ASC', filtre = '', enabled) {
    const res = await axios.$get(`${entite}/all-sort-dashboard`, {
      params: {
        page,
        size,
        sortBy,
        order,
        filtre,
        enabled,
      },
    });
    return new Promise((resolve) => resolve(res));
  },
  post(data) {
    return axios.$post(`${entite}`, convertirDateAffectationObjectToString(data));
  },
  put(id, data) {
    return axios.$put(`${entite}/${id}`, convertirDateAffectationObjectToString(data));
  },
  modifierInfosPersos(data) {
    return axios.$patch(`${entite}/modifier-infos-persos`, data);
  },
  annulerModifierInfosPersos() {
    return axios.$patch(`${entite}/annuler-modifier-infos-persos`);
  },
  renvoyerModifierInfosPersos() {
    return axios.$patch(`${entite}/renvoyer-modifier-infos-persos`);
  },
  getModifInfosPerso(token) {
    return axios.$get(`${entite}/infos-persos/${token}`);
  },
  delete(id) {
    return axios.$delete(`${entite}/${id}`);
  },
  me() {
    return axios.$get(`${entite}/me`);
  },
  creationProfils() {
    return axios.$get(`${entite}/creation-profils`);
  },
  valider(data) {
    return axios.$post(`${entite}/valider`, data);
  },
  debloquer(data) {
    return axios.$post(`${entite}/debloquer`, data);
  },
  supprimerInscription(data) {
    return axios.$post(`${entite}/supprimer-inscription`, data);
  },
  renouvellementInscriptionByToken(data) {
    return axios.$post(`${entite}/renew-inscription-by-token`, data);
  },
  inscriptionByToken(data) {
    return axios.$post(`${entite}/inscription-by-token`, data);
  },
  getConfirmationInscription(token) {
    return axios.$get(`${entite}/confirmation-inscription/${token}`);
  },
  postConfirmationInscription(data) {
    return axios.$post(`${entite}/confirmation-inscription`, data);
  },
  demandeNouveauMotDePasse(data) {
    return axios.$post(`${entite}/demande-nouveau-mdp`, data);
  },
  getChangementMotDePasse(token) {
    return axios.$get(`${entite}/changement-mdp/${token}`);
  },
  postChangementMotDePasse(data) {
    return axios.$post(`${entite}/changement-mdp`, data);
  },
  changementMotDePasseConnecter(data) {
    return axios.$patch(`${entite}/changement-mpd-connecte`, data);
  },
  modifierPartiellement(data) {
    return axios.$patch(`${entite}`, data);
  },
  modifierUserProfil(data) {
    return axios.$patch(`${entite}/default-user-profil/`, data);
  },
  alertes(limit = 10) {
    return axios.$get(`${entite}/alertes`, {
      params: {
        limit,
      },
    });
  },
  lireAlertes() {
    return axios.put(`${entite}/lire-alertes`);
  },
  cloturerUtilisateur(id, data) {
    return axios.$put(`${entite}/cloturer/${id}`, convertirDateFinAffectationObjectToString(data));
  },
  maParticipation(idActivite) {
    return axios.$get(`${entite}/activites/${idActivite}/ma-participation`);
  },
  mesAutresActivites(idActivite) {
    return axios.$get(`${entite}/activites/${idActivite}/mes-autres-activites`);
  },
  disponibilitesAnnuel(id, dateDebut) {
    return axios.$get(`${entite}/${id}/disponibilites-annuel/${dateDebut}`);
  },
});

export function utilisateur(axios, store) {
  const defaultId = () => {
    return store.state.auth.user && store.state.auth.user.id ? store.state.auth.user.id : null;
  };
  return (id = defaultId()) => ({
    async activitesEntre(dateDebut, dateFin, idTypeActivite, isMyActivities, idGroupe, filtre) {
      const activites = await axios.$get(`${entite}/${id}/activite/entre/${dateDebut}/${dateFin}`, {
        params: {
          idTypeActivite,
          isMyActivities,
          idGroupe,
          filtre,
        },
      });
      return new Promise((resolve) =>
        resolve(activites.map((activite) => addMonStatut(convertirActiviteDateStringToObject(activite)))),
      );
    },
    disponibilitesEntre(dateDebut, dateFin) {
      return axios.$get(`${entite}/${id}/disponibilite/entre/${dateDebut}/${dateFin}`);
    },
    postDisponibilite(data) {
      return axios.$post(`${entite}/disponibilite/`, data);
    },
    groupes() {
      return axios.$get(`${entite}/${id}/groupes`);
    },
    async activitesParticipation(page = 0, size = 30) {
      const res = await axios.$get(`${entite}/${id}/activite-participation/`, {
        params: {
          page,
          size,
        },
      });
      res.content = res.content.map((util) => convertirActiviteDateStringToObject(util));
      return new Promise((resolve) => resolve(res));
    },
    async activitesParticipationEntre(dateDebut, dateFin, page = 0, size = 30) {
      const res = await axios.$get(`${entite}/${id}/activite-participation/entre/${dateDebut}/${dateFin}`, {
        params: {
          page,
          size,
        },
      });
      res.content = res.content.map((util) => convertirActiviteDateStringToObject(util));
      return new Promise((resolve) => resolve(res));
    },
    organismesByEnfantOrderByHierarchie() {
      return axios.$get(`${entite}/${id}/organimes-parents-by-hierarchie/`);
    },
  });
}
