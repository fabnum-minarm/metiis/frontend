import { DateTime } from 'luxon';

export default {
  data() {
    return {
      monthNames: [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre',
      ],
      dayNames: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
    };
  },
  methods: {
    dateParse(date) {
      return DateTime.fromFormat(date, 'd/M/y').toJSDate();
    },
  },
};
