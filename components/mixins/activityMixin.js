import { DateTime } from 'luxon';

const classResolver = {
  methods: {
    resolveTypeActivite({ typeActivite }, prefix = '') {
      const res = {
        libelle: '',
        icon: '',
        class: `${prefix}none`,
      };
      const typeAct = this.appTypesActivites.find((type) => typeActivite.code === type.code);
      if (typeAct) {
        res.class = `${prefix}${typeAct.codeCss}`;
        res.libelle = typeAct.libelle;
        res.icon = typeAct.codeCssIcone;
      }
      return res;
    },
  },
};

const retourActivite = {
  computed: {
    queryMois() {
      return DateTime.fromJSDate(this.activite.dateDebut).toFormat('yyyy-MM');
    },
    page() {
      if (this.visu && this.visu === 'annuelle') {
        return this.visu;
      }
      return 'index';
    },
  },
};

const creationActivite = {
  data() {
    return {
      ouvert: false,
    };
  },
  methods: {
    async submit() {
      try {
        this.activite = await this.$api.activites.post(this.activite);
        this.$router.push(this.profilLink({ name: 'activites-id', params: { id: this.activite.id } }));
      } catch ({
        response: {
          status,
          data: { message },
        },
      }) {
        this.$buefy.notification.open({
          message,
          type: 'is-danger',
          duration: 5000,
        });
      }
    },
    async majEffectifPrevisionnel() {
      this.effectifPrevisionnel = await this.$api
        .organisme()
        .disponibilitesComplete(this.activite.dateDebut.toISOString(), this.activite.dateFin.toISOString());
    },
  },
};

function newActivity(store) {
  const activite = {
    nom: '',
    description: '',
    endDate: new Date(),
    startDate: new Date(),
    envoyerMailCreation: false,
    rappelNbJourAvantDebut: 0,
    rappelInApp: false,
    rappelEmail: false,
    rappelParticipationNbJourAvantDebut: 0,
    rappelParticipationInApp: false,
    rappelParticipationEmail: false,
    idParent: null,
    typeActivite:
      store.getters['context/typesActivites'] && store.getters['context/typesActivites'][0]
        ? { ...store.getters['context/typesActivites'][0] }
        : {
            id: null,
          },
    organisme: { ...store.getters['context/organisme'] },
    effectifs: store.getters['context/corps'].map((c) => ({
      nombre: 0,
      corps: { ...c },
    })),
    set dateDebut(date) {
      if (date) {
        date.setHours(8);
        this.startDate = date;
      }
    },
    get dateDebut() {
      return this.startDate;
    },
    set dateFin(date) {
      if (date) {
        date.setHours(18);
        this.endDate = date;
      }
    },
    get dateFin() {
      return this.endDate;
    },
  };
  activite.dateDebut = new Date();
  activite.dateFin = new Date();
  return activite;
}
function activityFromParent(store) {
  const activite = {
    envoyerMailCreation: false,
    rappelNbJourAvantDebut: 0,
    rappelInApp: false,
    rappelEmail: false,
    rappelParticipationNbJourAvantDebut: 0,
    rappelParticipationInApp: false,
    rappelParticipationEmail: false,
    organisme: { ...store.getters['context/organisme'] },
    effectifs: store.getters['context/corps'].map((c) => ({
      nombre: 0,
      corps: { ...c },
    })),
  };
  return activite;
}
function convertirActiviteDateStringToObject(activite) {
  return { ...activite, dateDebut: new Date(activite.dateDebut), dateFin: new Date(activite.dateFin) };
}
function addMonStatut(activite) {
  // ajout d'un attribut à activité pour le trie 'field' du b-table.
  let monStatut = '';
  if (activite.participants.length !== 0 && activite.participants[0].dernierAvancement) {
    monStatut = activite.participants[0].dernierAvancement.choix.statut.nomSingulier;
  }
  return { ...activite, monStatut };
}

function convertirActiviteDateObjectToString(activite) {
  const activiteReq = {
    ...activite,
    dateDebut: activite.dateDebut.toISOString(),
    dateFin: activite.dateFin.toISOString(),
  };
  delete activiteReq.startDate;
  delete activiteReq.endDate;
  return activiteReq;
}

export {
  classResolver,
  retourActivite,
  newActivity,
  activityFromParent,
  convertirActiviteDateStringToObject,
  convertirActiviteDateObjectToString,
  addMonStatut,
  creationActivite,
};
