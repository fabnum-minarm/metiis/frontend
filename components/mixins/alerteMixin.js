import { Duration, DateTime } from 'luxon';

export default {
  methods: {
    calculerAncienneteDate(date) {
      const luxonDate = DateTime.fromISO(date);
      const now = DateTime.fromJSDate(new Date());
      const { years, months, days, hours, minutes, seconds } = Duration.fromMillis(
        now.toMillis() - luxonDate.toMillis(),
      )
        .shiftTo('years', 'months', 'days', 'hours', 'minutes', 'seconds')
        .toObject();

      const ilya = 'Il y a';
      if (years > 1) {
        return `${ilya} ${years} ans`;
      }
      if (years > 0) {
        return `${ilya} ${years} an`;
      }
      if (months > 0) {
        return `${ilya} ${months} mois`;
      }
      if (days > 1) {
        return `${ilya} ${days} jours`;
      }
      if (days > 0) {
        return `${ilya} ${days} jour`;
      }
      if (hours > 1) {
        return `${ilya} ${hours} heures`;
      }
      if (hours > 0) {
        return `${ilya} ${hours} heure`;
      }
      if (minutes > 1) {
        return `${ilya} ${minutes} minutes`;
      }
      if (minutes > 0) {
        return `${ilya} ${minutes} minute`;
      }
      if (seconds > 1) {
        return `${ilya} ${parseInt(seconds, 10)} secondes`;
      }
      return `${ilya} 1 seconde`;
    },
    tronquerAvec3Points(texte, longueur) {
      let retour = texte;
      if (retour && retour.length > longueur) {
        retour = retour.substr(0, longueur).concat('...');
      }
      return retour;
    },
  },
};
