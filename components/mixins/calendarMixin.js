import { DateTime } from 'luxon';

const calendarParent = {
  methods: {
    async onViewChange({ firstCellDate, lastCellDate, startDate }) {
      const isEmploye = this.$store.getters['context/isEmploye'];
      let activitesReq = null;
      let disponibilitesReq = null;
      if (isEmploye) {
        activitesReq = this.$api
          .utilisateur()
          .activitesEntre(
            firstCellDate.toISOString(),
            lastCellDate.toISOString(),
            this.typeActiviteSelected,
            this.isMyActivities,
          );

        disponibilitesReq = this.$api
          .utilisateur()
          .disponibilitesEntre(firstCellDate.toISOString(), lastCellDate.toISOString());
      } else {
        activitesReq = this.$api
          .organisme()
          .activitesEntre(firstCellDate.toISOString(), lastCellDate.toISOString(), this.typeActiviteSelected);

        disponibilitesReq = this.$api
          .organisme()
          .disponibilitesEntre(firstCellDate.toISOString(), lastCellDate.toISOString());
      }

      [this.activites, this.disponibilites, this.activiteAnnuel] = await Promise.all([activitesReq, disponibilitesReq]);
      this.currentMonthDate = startDate;
    },
    onDateSelected({ selectedDateStart, selectedDateEnd }) {
      this.activite.dateDebut = selectedDateStart;
      this.activite.dateFin = selectedDateEnd;
    },
  },
};

function requetesAsyncDataCommunes(
  { $api, store, idtypeActivite, isMyActivities },
  moisCourant = new Date(),
  ...requetesSupplementaires
) {
  const isEmploye = store.getters['context/isEmploye'];
  const myGroups = store.getters['context/groupesSelected'];
  const start = DateTime.fromJSDate(moisCourant)
    .startOf('month')
    .startOf('week');

  const end = DateTime.fromJSDate(moisCourant)
    .startOf('month')
    .plus({ weeks: 5 })
    .endOf('week');
  let activitesReq;
  let disponibilitesReq;

  if (isEmploye) {
    activitesReq = $api
      .utilisateur()
      .activitesEntre(start.toISO(), end.toISO(), idtypeActivite, isMyActivities, myGroups);
    disponibilitesReq = $api.utilisateur().disponibilitesEntre(start.toISO(), end.toISO());
  } else {
    activitesReq = $api.organisme().activitesEntre(start.toISO(), end.toISO(), idtypeActivite, myGroups);
    disponibilitesReq = $api.organisme().disponibilitesEntre(start.toISO(), end.toISO());
  }
  return Promise.all([activitesReq, disponibilitesReq, ...requetesSupplementaires]);
}
export { calendarParent, requetesAsyncDataCommunes };
