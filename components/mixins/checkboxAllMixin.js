function newCheckboxAllEtat() {
  return {
    checked: false,
    indeterminate: false,
  };
}
export default newCheckboxAllEtat;
