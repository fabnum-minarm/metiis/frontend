function needVerification({ canActiviteWithOtherActivite }) {
  return !canActiviteWithOtherActivite;
}
export default needVerification;
