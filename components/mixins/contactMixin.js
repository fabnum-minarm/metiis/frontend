function newContact() {
  return {
    nom: null,
    prenom: null,
    email: null,
    objet: null,
    message: null,
    dateCreation: null,
    lu: false,
    traite: false,
  };
}

export default newContact;
