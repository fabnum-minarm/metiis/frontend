function createExportLink({ data }, fileName) {
  const url = window.URL.createObjectURL(new Blob([data]));
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', fileName);
  document.body.appendChild(link);
  link.click();
}
function dateFileFormat(date) {
  const options1 = { year: 'numeric', month: 'numeric', day: 'numeric' };

  const dateTimeFormat1 = new Intl.DateTimeFormat('fr-FR', options1);
  return dateTimeFormat1.format(date);
}
export { createExportLink, dateFileFormat };
