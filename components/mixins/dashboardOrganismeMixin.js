export default {
  methods: {
    async majOrganismes() {
      this.organismes = await this.$api.organismes.organismesByParentAndTypeOrganisme(this.organisme.idTypeOrganisme);
    },
  },
};
