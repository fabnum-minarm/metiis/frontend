import { DateTime } from 'luxon';

function newDisponibilite() {
  return {
    commentaire: '',
    dateFin: new DateTime(),
    dateDebut: new DateTime(),
    id: 0,
    typeDisponibilite: {
      code: '',
      estDisponible: true,
      id: 0,
      libelle: '',
    },
    utilisateur: {},
  };
}

function convertirDateDisponibiliteStringToObject(disponibilite) {
  return {
    ...disponibilite,
    dateFin: DateTime.fromISO(disponibilite.dateFin, { zone: 'utc' }),
    dateDebut: DateTime.fromISO(disponibilite.dateDebut, { zone: 'utc' }),
  };
}
export { convertirDateDisponibiliteStringToObject };
export default newDisponibilite;
