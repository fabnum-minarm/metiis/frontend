function newEtape() {
  return {
    code: '',
    nom: '',
    nomPluriel: '',
    ordre: null,
    description: '',
    descriptionPluriel: '',
  };
}
export default newEtape;
