function newEvaluation() {
  return {
    noteBesoin: null,
    noteUtilisation: null,
    commentaire: '',
    utilisateur: {
      id: null,
      nom: '',
      prenom: '',
    },
  };
}
export default newEvaluation;
