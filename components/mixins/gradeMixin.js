function newGrade() {
  return {
    code: '',
    corps: {
      code: '',
      id: null,
      lettre: '',
      libelle: '',
      ordre: null,
    },
    id: null,
    libelle: '',
    ordre: null,
  };
}
export default newGrade;
