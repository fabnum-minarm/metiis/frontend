import debounce from 'lodash/debounce';

function newGroupePersonnaliser() {
  return {
    nom: null,
    proprietaire: null,
    groupesPersonnaliserDetails: [],
  };
}

const groupePersonnaliserMixin = {
  data() {
    return {
      idCorps: null,
      filtre: '',
      isFetching: false,
      selected: null,
      data: [],
      ouvert: false,
    };
  },
  methods: {
    retourGroupe() {
      this.$router.push(this.profilLink({ name: 'pax-groupe' }));
    },
    isSelected(utilisateur) {
      if (this.utilisateursSelected.includes(utilisateur)) {
        return 'mt-list-item-selected';
      }
      return '';
    },
    isAutoCompleteSelected(utilisateur) {
      // obligé de testé avec l'id, car meme si c'est le meme objet , le teste ne marchait pas avec juste un includes.
      const search = this.utilisateursSelected.find((value) => value.id === utilisateur.id);
      if (search) {
        return 'mt-list-item-selected';
      }
      return '';
    },
    selectionnerAll(value) {
      this.utilisateursSelected = [];
      if (value) {
        this.utilisateurs.filtrer.forEach((utilisateur) => {
          this.utilisateursSelected.push(utilisateur);
        });
      }
    },
    selectAutoComplete(option) {
      this.selected = option;
    },
    filtreCategorie(corps) {
      this.idCorps = corps.id;
      this.refresh();
    },
    viderFiltre() {
      this.idCorps = null;
      this.refresh();
    },
    async refresh() {
      this.utilisateurs = await this.$api.organisme().utilisateursFilter(this.idCorps);
    },
    ajouter(utilisateur) {
      if (!this.utilisateursSelected.find((value) => value.id === utilisateur.id)) {
        this.utilisateursSelected.push(utilisateur);
      }
    },
    supprimer(utilisateur) {
      if (this.utilisateursSelected.find((value) => value.id === utilisateur.id)) {
        this.utilisateursSelected = this.utilisateursSelected.filter((value) => value.id !== utilisateur.id);
      }
    },
    getAsyncData: debounce(async function getAsyncData(search) {
      this.isFetching = true;
      try {
        if (search != null && search.length > 0) {
          this.data = await this.$api.organisme().rechercheUtilisateurs(search);
        } else {
          this.data = [];
        }
      } catch (e) {
        this.$buefy.toast.open({
          message: 'Une erreur est survenue lors de la récupération des données.',
          type: 'is-danger',
        });
      } finally {
        this.isFetching = false;
      }
    }, 500),
  },
};

export { newGroupePersonnaliser, groupePersonnaliserMixin };
