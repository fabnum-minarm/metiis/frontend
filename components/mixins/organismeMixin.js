function newOrganisme() {
  return {
    id: null,
    code: '',
    libelle: '',
    credo: '',
    idparent: null,
    idTypeOrganisme: null,
  };
}
export default newOrganisme;
