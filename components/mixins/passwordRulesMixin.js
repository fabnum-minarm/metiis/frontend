export default {
  data() {
    return {
      rules: [
        {
          nom: 'Au moins 12 caractères.',
          condition: '^.{12,}$',
          valid: null,
        },
        {
          nom: 'Au moins une majuscule.',
          condition: '[A-Z]{1,}',
          valid: null,
        },
        {
          nom: 'Au moins une minuscule.',
          condition: '[a-z]{1,}',
          valid: null,
        },
        {
          nom: 'Au moins un caractère spécial.',
          condition: '[^A-Za-z0-9]{1,}',
          valid: null,
        },
        {
          nom: 'Au moins un chiffre.',
          condition: '[0-9]{1,}',
          valid: null,
        },
        {
          nom: 'Au moins une lettre.',
          condition: '[A-Za-z]{1,}',
          valid: null,
        },
      ],
    };
  },
};
