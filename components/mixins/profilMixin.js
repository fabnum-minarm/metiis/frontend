function newProfil() {
  return {
    id: null,
    code: '',
    description: '',
    typeOrganisme: { id: null },
    isDefault: false,
    roles: [],
  };
}
async function getData(context, profil) {
  const [typesOrganismes, roles] = await Promise.all([context.$api.typesOrganismes.all(), context.$api.users.roles()]);
  typesOrganismes.unshift({ libelle: 'Aucun', id: null });
  const selectedRoles = {};
  roles.forEach((role) => {
    selectedRoles[role] = !!profil.roles.includes(role);
  });
  return {
    profil,
    roles,
    selectedRoles,
    typesOrganismes,
  };
}
export { newProfil, getData };
