import { DateTime } from 'luxon';

function newStatistique() {
  return {
    jourDisponible: 0,
    jourIndisponible: 0,
    jourNonRenseigne: 0,
    jourEnActivite: 0,
    activitesRealiser: [],
    activites: [],
    avancementActivite: [],
  };
}

const statistiqueMixin = {
  computed: {
    dateInError() {
      if (!this.start || !this.end) {
        return false;
      }
      return !DateTime.fromJSDate(this.start).hasSame(DateTime.fromJSDate(this.end), 'day') && this.end < this.start;
    },
  },
  methods: {
    dateFormatter(date) {
      if (!date) {
        return '';
      }
      return DateTime.fromISO(date)
        .setLocale('fr')
        .toFormat('dd/MM/yyyy HH:mm');
    },
  },
};

export { statistiqueMixin, newStatistique };
