export default {
  methods: {
    setToken(token) {
      const TOKEN_STORAGE = `${this.$auth.options.token.prefix}${this.$auth.options.defaultStrategy}`;
      const TOKEN_PREFIX = `${this.$auth.strategies.local.options.tokenType} `;
      this.$auth.$storage.setUniversal(TOKEN_STORAGE, `${TOKEN_PREFIX}${token}`);
      this.$axios.setHeader(this.$auth.strategies.local.options.tokenName, `${TOKEN_PREFIX}${token}`);
    },
  },
};
