function newTypeDisponibilite() {
  return {
    code: '',
    libelle: '',
    estDisponible: null,
  };
}

export default newTypeDisponibilite;
