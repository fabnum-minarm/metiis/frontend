function newTypeOrganisme() {
  return {
    code: '',
    libelle: '',
    ordre: null,
    pourAffectation: false,
    idTypeOrganismeParent: null,
  };
}
export default newTypeOrganisme;
