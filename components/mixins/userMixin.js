import { mapGetters } from 'vuex';

export default {
  computed: {
    utilisateur() {
      if (!this.$store.state.auth.user || !this.$store.state.auth.user.id) {
        return null;
      }
      return this.$store.state.auth.user;
    },
    ...mapGetters({
      profil: 'context/profil',
      isEmploye: 'context/isEmploye',
      isSuperEmploye: 'context/isSuperEmploye',
      isEmployeur: 'context/isEmployeur',
      isAdmin: 'context/isAdmin',
      isAdminLocal: 'context/isAdminLocal',
      listeOrganismes: 'context/organismes',
      organisme: 'context/organisme',
    }),
  },
};
