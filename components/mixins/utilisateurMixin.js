function newUtilisateur() {
  const utilisateur = {
    affectation: {
      id: null,
      dateAffectation: new Date(),
      dateFinAffectation: null,
      organisme: {
        id: null,
        idparent: null,
      },
      posteRo: {
        id: null,
      },
    },
    email: '',
    enabled: false,
    accountNonLocked: true,
    corps: {
      id: null,
    },
    id: null,
    nom: '',
    prenom: '',
    initial: '',
    profilCourant: {
      id: null,
      code: '',
    },
    telephone: '',
    userProfils: [],
    username: '',
    password: '',
    passwordconfirmation: '',
  };
  return utilisateur;
}

function convertirDateAffectationStringToObject(utilisateur) {
  const affectation = {
    ...utilisateur.affectation,
    dateAffectation: new Date(utilisateur.affectation.dateAffectation),
  };
  return { ...utilisateur, affectation };
}
function convertirDateAffectationObjectToString(utilisateur) {
  const affectation = {
    ...utilisateur.affectation,
    dateAffectation: utilisateur.affectation.dateAffectation.toISOString(),
  };
  return { ...utilisateur, affectation };
}

function convertirDateFinAffectationObjectToString(utilisateur) {
  const affectation = {
    ...utilisateur.affectation,
    dateFinAffectation: utilisateur.affectation.dateFinAffectation.toISOString(),
  };
  return { ...utilisateur, affectation };
}

function convertirDateActivationStringToObject(utilisateur) {
  return { ...utilisateur, dateActif: new Date(utilisateur.dateActif) };
}

export {
  newUtilisateur,
  convertirDateAffectationObjectToString,
  convertirDateAffectationStringToObject,
  convertirDateFinAffectationObjectToString,
  convertirDateActivationStringToObject,
};
