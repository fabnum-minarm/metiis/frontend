import { NotificationProgrammatic as Notification } from 'buefy';

export default async function({ params, store, redirect }) {
  if (params && params.profil) {
    if (!store.getters['context/profils'].includes(params.profil)) {
      Notification.open({
        message:
          "Votre compte ne contient pas le profil adéquat pour accéder à cette page.<br>Vous avez été redirigé vers l'accueil",
        type: 'is-danger',
        duration: 5000,
      });
      const profil = store.state.auth.user.profilCourant.code;
      await store.dispatch('context/setProfil', profil);
      redirect(`/${profil}`);
    }
  }
}
