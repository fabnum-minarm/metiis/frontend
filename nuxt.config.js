export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Metiis',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Metiis - Un lien qui transforme le potentiel de la réserve en une force pour l’armée de terre',
      },
    ],
    link: [
      { rel: 'apple-touch-icon', href: '/metiis-icon-192x192.png' },
      { rel: 'icon', sizes: '128×128', href: '/metiis-icon-192x192.png' },
      { rel: 'icon', sizes: '192×192', href: '/metiis-icon-192x192.png' },
    ],
  },
  pwa: {
    manifest: {
      name: 'Metiis',
      short_name: 'metiis',
      description: 'Metiis - Un lien qui transforme le potentiel de la réserve en une force pour l’armée de terre',
      lang: 'fr',
    },
  },

  router: {
    middleware: ['auth', 'profil'],
  },

  serverMiddleware: ['~/server-middleware/headers'],
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [{ src: '~assets/css/main.scss', lang: 'scss' }],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/api', '~/plugins/detect-mobile.js', '~/plugins/matomo.js'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/pwa',
    'nuxt-clipboard2',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  buefy: { css: false, materialDesignIcons: false },
  auth: {
    plugins: [
      { src: '~/plugins/context.js', ssr: false },
      { src: '~/plugins/auth-renew.js', ssr: false },
    ],
    rewriteRedirects: false,
    redirect: {
      login: '/connexion',
      logout: '/connexion',
      callback: '/connexion',
      home: false,
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/v1/auth/signin', method: 'post', propertyName: 'token' },
          user: { url: '/v1/utilisateur/me', method: 'get', propertyName: false },
          logout: null,
        },
      },
    },
  },
  server: {
    host: '0.0.0.0',
  },

  env: {
    matomoHost: process.env.MATOMO_HOST || null,
    matomoSideId: process.env.MATOMO_SITEID || null,
  },
};
