// eslint-disable-next-line import/no-extraneous-dependencies
import { NotificationProgrammatic as Notification } from 'buefy';

export default function({ app: { $auth, $api, $axios } }) {
  if (process.client && !!window && !!JSON) {
    const TOKEN_STORAGE = `${$auth.options.token.prefix}${$auth.options.defaultStrategy}`;
    const TOKEN_PREFIX = `${$auth.strategies.local.options.tokenType} `;
    const autoRenew = () => {
      const [, payload] = $auth.$storage
        .getUniversal(TOKEN_STORAGE)
        .replace(TOKEN_PREFIX, '')
        .split('.');
      const { exp, iat } = JSON.parse(window.atob(payload));
      const cur = Math.floor(Date.now() / 1000);
      const duration = exp - iat;
      const renewTrigger = duration / 2;
      const expIn = exp - cur;
      if (expIn <= 0) {
        $auth.logout();
      } else if (expIn <= renewTrigger) {
        $api.auth
          .renew()
          .then(({ data: { token } }) => {
            $auth.$storage.setUniversal(TOKEN_STORAGE, `${TOKEN_PREFIX}${token}`);
            $axios.setHeader($auth.strategies.local.options.tokenName, `${TOKEN_PREFIX}${token}`);
            autoRenew();
          })
          .catch(() => {
            Notification.open({
              message: 'Impossible de prolonger la session, le jeton a expiré. Vous avez été déconnecté.',
              type: 'is-danger',
              duration: 5000,
            });
            $auth.logout();
          });
      } else {
        setTimeout(autoRenew, (expIn - renewTrigger) * 1000);
      }
    };

    if ($auth.loggedIn) {
      autoRenew();
    }

    $auth.$storage.watchState('loggedIn', (loggedIn) => {
      if (loggedIn) {
        autoRenew();
      }
    });
  }
}
