import Vue from 'vue';
import { mapGetters } from 'vuex';

export default async function({ app: { $auth, $api }, store, params }) {
  async function initOrganisme() {
    if (params && params.profil) {
      await store.dispatch('context/setProfil', params.profil);
    } else {
      await store.dispatch('context/setProfil', store.state.auth.user.profilCourant.code);
    }
    await store.dispatch(
      'context/setProfils',
      store.state.auth.user.userProfils.map((profil) => profil.profil.code),
    );
  }

  async function initOrganismes() {
    const monOrganisme = store.state.auth.user.affectation.organisme;
    if (store.getters['context/isAdminLocal']) {
      const organismes = await $api.organismes.affectableAllowed();
      await store.dispatch('context/setOrganismes', organismes);
      let index = 0;
      for (let i = 0; i < organismes.length; i += 1) {
        if (organismes[i].id === monOrganisme.id) {
          index = i;
          break;
        }
      }
      await store.dispatch('context/setOrganisme', organismes[index]);
    } else {
      await store.dispatch('context/setOrganisme', monOrganisme);
    }
  }

  async function loadGlobal() {
    const { content: corps } = await $api.corps.all(0, 50);
    store.dispatch('context/setCorps', corps);
    const { content: typeActivite } = await $api.typesActivites.allPage(0, 50);
    store.dispatch('context/setTypesActivites', typeActivite);
    store.dispatch('context/setTypesDisponibilites', await $api.typesDisponibilites.all());
    store.dispatch('context/setTypesOrganismes', await $api.typesOrganismes.all());
  }

  async function loadNouveaute() {
    const nouveautes = await $api.nouveautes.me();
    store.dispatch('context/setNouveautes', nouveautes);
  }

  if ($auth.loggedIn) {
    await initOrganisme();
    await initOrganismes();
    await loadGlobal();
    await loadNouveaute();
  }

  $auth.$storage.watchState('loggedIn', async (loggedIn) => {
    if (loggedIn) {
      await initOrganisme();
      await initOrganismes();
      await loadGlobal();
      await loadNouveaute();
    }
  });
  Vue.mixin({
    computed: {
      ...mapGetters({
        appCorps: 'context/corps',
        appTypesActivites: 'context/typesActivites',
        appTypesOrganismes: 'context/typesOrganismes',
      }),
    },
    methods: {
      profilLink(linkObjet) {
        return {
          ...linkObjet,
          name: linkObjet.name === '' || linkObjet.name === 'index' ? `profil` : `profil-${linkObjet.name}`,
          params: {
            ...linkObjet.params,
            profil: this.$store.getters['context/profil'],
          },
        };
      },
    },
  });
}
