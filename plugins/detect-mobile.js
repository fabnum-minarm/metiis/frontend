export default ({ store }) => {
  if (process.client) {
    const isMobile = window.matchMedia('only screen and (max-width: 768px)').matches;
    store.dispatch('context/setIsMobile', isMobile);
  }
};
