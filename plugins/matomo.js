import Vue from 'vue';
import VueMatomo from 'vue-matomo';

export default ({ app }) => {
  if (process.env.matomoHost && process.env.matomoSideId) {
    Vue.use(VueMatomo, {
      router: app.router,
      host: process.env.matomoHost,
      siteId: process.env.matomoSideId,
    });
  }
};
