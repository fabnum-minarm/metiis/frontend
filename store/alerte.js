// We've to disable param reassign, because it's the common behavior of vuex
/* eslint-disable no-param-reassign,no-shadow */

export const state = () => ({
  nonLue: 0,
});

export const getters = {
  nonLue: (s) => s.nonLue,
};

export const mutations = {
  setNonLu: (s, nonLue = 0) => {
    s.nonLue = nonLue;
  },
};

export const actions = {
  setNonLu({ commit }, nonLue = 0) {
    commit('setNonLu', nonLue);
  },
};
