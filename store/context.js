// We've to disable param reassign, because it's the common behavior of vuex
/* eslint-disable no-param-reassign,no-shadow */

export const state = () => ({
  isMobile: false,
  profil: 'employe',
  organisme: null,
  organismes: [],
  profils: [],
  typesActivites: [],
  typesOrganismes: [],
  corps: [],
  typesDisponibilites: [],
  nouveautes: [],
  groupesSelected: [],
});

export const getters = {
  isMobile: (s) => s.isMobile,
  profil: (s) => s.profil,
  organisme: (s) => s.organisme,
  organismes: (s) => s.organismes,
  profils: (s) => s.profils,
  typesActivites: (s) => s.typesActivites,
  typesOrganismes: (s) => s.typesOrganismes,
  typesDisponibilites: (s) => s.typesDisponibilites,
  corps: (s) => s.corps,
  isEmploye: (s) => s.profil === 'employe' || s.profil === 'super_employe',
  isSuperEmploye: (s) => s.profil === 'super_employe',
  isEmployeur: (s) => s.profil === 'employeur' || s.profil === 'admin' || s.profil === 'super_employeur',
  isSuperEmployeur: (s) => s.profil === 'super_employeur',
  isAdmin: (s) => s.profil === 'admin',
  isAdminLocal: (s) => s.profil === 'admin_brigade' || s.profil === 'admin_regiment' || s.profil === 'admin',
  isAdminBrigade: (s) => s.profil === 'admin_brigade' || s.profil === 'admin',
  hasEmploye: (s) => s.profils.includes('employe'),
  hasEmployeur: (s) => s.profils.includes('employeur'),
  hasAdmin: (s) => s.profils.includes('admin'),
  nouveautes: (s) => s.nouveautes,
  groupesSelected: (s) => s.groupesSelected,
};

export const mutations = {
  setIsMobile: (s, isMobile = false) => {
    s.isMobile = isMobile;
  },
  setProfil: (s, profil) => {
    s.profil = profil;
  },
  setOrganisme: (s, organisme) => {
    s.organisme = organisme;
  },
  setOrganismes: (s, organismes) => {
    s.organismes = organismes;
  },
  setProfils: (s, profils) => {
    s.profils = profils;
  },
  setTypesActivites: (s, typesActivites) => {
    s.typesActivites = typesActivites;
  },
  setTypesOrganismes: (s, typesOrganismes) => {
    s.typesOrganismes = typesOrganismes;
  },
  setTypesDisponibilites: (s, typesDisponibilites) => {
    s.typesDisponibilites = typesDisponibilites;
  },
  setCorps: (s, corps) => {
    s.corps = corps.sort((a, b) => a.ordre - b.ordre);
  },
  setNouveautes: (s, nouveautes) => {
    s.nouveautes = nouveautes;
  },
  setGroupesSelected: (s, groupesSelected) => {
    s.groupesSelected = groupesSelected;
  },
};

export const actions = {
  setIsMobile({ commit }, isMobile = false) {
    commit('setIsMobile', isMobile);
  },
  setProfil({ commit }, profil) {
    commit('setProfil', profil);
  },
  setOrganisme({ commit }, organisme) {
    commit('setOrganisme', organisme);
  },
  setOrganismes({ commit }, organismes) {
    commit('setOrganismes', organismes);
  },
  setProfils({ commit }, profils) {
    commit('setProfils', profils);
  },
  setTypesActivites({ commit }, typesActivites) {
    commit('setTypesActivites', typesActivites);
  },
  setTypesOrganismes({ commit }, typesOrganismes) {
    commit('setTypesOrganismes', typesOrganismes);
  },
  setTypesDisponibilites({ commit }, typesDisponibilites) {
    commit('setTypesDisponibilites', typesDisponibilites);
  },
  setCorps({ commit }, corps) {
    commit('setCorps', corps);
  },
  setNouveautes({ commit }, nouveautes) {
    commit('setNouveautes', nouveautes);
  },
  setGroupesSelected({ commit }, groupesSelected) {
    commit('setGroupesSelected', groupesSelected);
  },
};
