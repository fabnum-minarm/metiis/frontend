/*
Fake webpack config to help IntelliJ resolve '~' alias path to __dirname
In IntelliJ settings : Preferences | Languages & Frameworks | JavaScript | Webpack
webpack configuration file : Choose this file
*/

module.exports = {
  resolve: {
    alias: {
      '~': __dirname,
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: [
          'vue-loader',
          'style-loader', // creates style nodes from JS strings
          'css-loader', // translates CSS into CommonJS
          'sass-loader', // compiles Sass to CSS, using Node Sass by default
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader', // creates style nodes from JS strings
          'css-loader', // translates CSS into CommonJS
          'sass-loader', // compiles Sass to CSS, using Node Sass by default
        ],
      },
    ],
  },
};
